<?php 
class User extends AbstractEntity {
	private $firstName;
	private $lastName;
	private $email;
	private $password;

	public function __construct($firstName, $lastName, $email, $password) {
		//$this->firstName = $firstName;
		//$this->lastName = $lastName;
		//$this->email = $email;
		//$this->password = $password;
		$this->setFirstName($firstName);
		$this->setLastName($lastName);
		$this->setEmail($email);
		$this->setPassword($password);
	}

	public function setFirstName($firstName) {
		$this->firstName = $firstName;
	}

	public function getFirstName() {
		return $this->firstName;
	}

	public function setLastName($lastName) {
		$this->lastName = $lastName;
	}

	public function getLastName() {
		return $this->lastName;
	}

	public function setEmail($email) {
		$this->email = $email;
	}

	public function getEmail() {
		return $this->email;
	}

	public function setPassword($password) {
		$this->password = $password;
	}

	public function getPassword() {
		return $this->password;
	}
}
?>