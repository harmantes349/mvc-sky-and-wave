<?php
//include('config.php');
// On met dans une variable le nombre de messages qu'on veut par page
$nombreDeMessagesParPage = 8;
// On récupère le nombre total de messages

$retour = $bdd->query('SELECT COUNT(*) AS nb_messages FROM saisi_offre');

$donnees = $retour->fetch();
$totalDesMessages = $donnees['nb_messages'];
// On calcule le nombre de pages à créer
$nombreDePages  = ceil($totalDesMessages / $nombreDeMessagesParPage);
// Puis on fait une boucle pour écrire les liens vers chacune des pages
if (isset($_GET['page'])) {
    $page = $_GET['page']; // On récupère le numéro de la page indiqué dans l'adresse
} else { // La variable n'existe pas, c'est la première fois qu'on charge la page
      $page = 1; // On se met sur la page 1 (par défaut)
}
// On calcule le numéro du premier message qu'on prend pour le LIMIT de MySQL
$premierMessageAafficher = ($page - 1) * $nombreDeMessagesParPage;
// Récupération et affichage des offres !
//param Recherche
// par lieu RechercheLieu

if (isset($_GET['recherche'])) {
    $q = $_GET['q'];
    $column = $_GET['column'];
    if ($column == "" || ($column != "lieu" && $column != "titre")) {
        $column = "lieu";
    }

    $reponse = $bdd->query('SELECT id_saisi_offre, message, prix, titre, localisation, lieu, Image, users.username
  FROM
  saisi_offre AS SO
  INNER JOIN users ON SO.id_users = users.id
  WHERE '.$column.'
  LIKE "%'.$q.'%"');
} else {
    $reponse = $bdd->query('SELECT id_saisi_offre, message, prix, titre, localisation, lieu, Image, users.username
  FROM
  saisi_offre AS SO
  INNER JOIN users ON SO.id_users = users.id
  ORDER BY id DESC LIMIT '.$premierMessageAafficher.','.$nombreDeMessagesParPage.'
');
}
//retravailler sur la limite !

while ($donnees = $reponse->fetch()) {
    if (empty($donnees['Image'])) {
        $src = '/ProjectPlanche/images/planche.svg';
    } else {
        $src = '/ProjectPlanche/uploads/' . $donnees['Image'];
    }
    if (empty($donnees['titre'])) {
        $titre = '....';
    } else {
        $titre = $donnees['titre'];
    } ?>

     <li class="<?php echo htmlspecialchars($donnees['localisation']); ?> card" style="width: 23rem;height:27rem;">
<a align="center" href="<?php echo htmlspecialchars($src); ?>"><img class="card-img-top" alt="fORMAT NON SUPORTER !" class="imagePlanche" style="background:#fff;" src="<?php echo htmlspecialchars($src); ?>"/></a>
  <div class="card-body">

        <strong class="text-primary"><p class="card-text">Titre : </strong><?php echo htmlspecialchars($titre); ?></p>
        <strong class="text-primary"><p class="card-text">Auteur : </strong><?php echo htmlspecialchars($donnees['username']); ?></p>
          <p class="card-text messageUtil"><strong class="text-info">Description : <?php echo '<a class="#" href="php/zoom.php?id_saisi_offre='.htmlspecialchars($donnees['id_saisi_offre']).'">      <i class="text-info fa fa-question fa-2x"></i></a>'; ?></strong><?php echo htmlspecialchars($donnees['message']); ?></p>
          <p><strong class="text-info card-text">Prix :</strong><span><?php echo htmlspecialchars($donnees['prix']); ?></span></p>
          <p><strong class="text-info card-text">Lieu :</strong><?php echo htmlspecialchars($donnees['lieu']); ?></p>

<?php
if (isset($_SESSION['username'])and $_SESSION['username'] != $donnees['username']) {
        ?>
        <hr>
  <a onclick="window.location = '<?php echo 'php/mise_en_relation.php?id_saisi_offre='.htmlspecialchars($donnees['id_saisi_offre'])?>';"  value="#" class="btn btn-social btn-success"><span class="fa fa-anchor"></span>Message privé !</a>
  <a onclick="window.location = '<?php echo 'php/zoom.php?id_saisi_offre='.htmlspecialchars($donnees['id_saisi_offre'])?>';"  value="#" class="btn btn-social btn-info"><span class="fa fa-question"></span>Question ?</a>
</div>


</li>
<?php        //dispel le bouton apres requete !
    } ?>

<?php
}
$reponse->closeCursor();
?>
