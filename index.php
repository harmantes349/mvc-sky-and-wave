<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
	<?php
		// WEBROOT => dossier du projet de la racine serveur
		define('WEBROOT',str_replace('index.php', '', $_SERVER['SCRIPT_NAME']));
		// ROOT => dossier du projet de la racine du disque dur
		define('ROOT',str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']));

	 ?>
 	<head>
 	<meta charset="UTF-8" />
 	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
 	<!-- Metas Page details-->
 	<title>Sky-&-Wave</title>
 	<meta name="description" content="sky&wind">
 	<meta name="author" content="Julioo"
 	<!-- Mobile Specific Metas-->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 	<!--main style-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
 	<link rel="stylesheet" type="text/css" media="screen" href="<?= WEBROOT ?>css/main.css">
 	<link rel="stylesheet" type="text/css" media="screen" href="<?= WEBROOT ?>css/responsive.css">
 	  <script type="text/javascript" src="css/bootstrap.js"></script>
 	<!--google font style-->
 	<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
 	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

 	</head>
 	<!--head fin
 	-->


<body>
	<header>
		
		<?php
		if (isset($log)) {
			echo $log;
		}
		if (isset($user)) {
			echo $user;
		}
		 ?>
		<?php
		if (isset($_SESSION['id'])) {
			echo '<div id="tete">
					<p class="infoLog">Bienvenue,
						</br>
						<span class="fa fa-user" style="font-size:1.3em; color:#fff;"></span>
						<span class="t_menu" style="font-size:1.3em;">'.htmlentities($_SESSION['prenom']).'   '.htmlentities($_SESSION['nom']).'</span>
					</p>
				   </div>';
		} else {


	 } ?>
	 <!--image-->
	 <div align="center">
		<img  alt="Sky-and-Wave" id="titre_main" src="<?= WEBROOT ?>img/titre_sky.svg">
		<h1 style="display: none;">SkyandWave</h1>
		</div>
	 
	</header>
	<?php
		// Init
		require_once('vue/navbar/navbar.php');
		require_once('core/bdd.php');
		require_once('core/controller.php');
		require_once('core/abstractEntity.php');

		// Page par default
		if (isset($_GET['p'])) {
			if ($_GET['p'] == "") {
				$_GET['p'] = "controller/action";
			}
		} else {
			$_GET['p'] = "controller/action";
		}

		// Chargement du controleur
		// $tabControlleur est le tableau contenant tout les nom de controlleurs accepté par l'appli
		//ici il faut ecrire dans l'array le nom des controller utilisé !
		$tabControlleur = array('User','Offer');
		$param = explode("/",$_GET['p']);

		// Si le nom de controlleur venant de l'url est dans le $tabControlleur
		if (in_array($param[0], $tabControlleur)) {
			$controller = $param[0];
			if (isset($param[1])) {
				$action = $param[1];
			} else {
				$action = 'index';
			}
			// Chargement du controlleur
			require_once('controlleur/'.$controller.'.ctrl.php');
			// Nomage de la classe du controlleur
			$controller = 'Ctrl'.$controller;
			// Intanciation du controlleur
			$controller = new $controller();

			// Execution de l' $action du $controller avec les $param supplementaire si existant
			// Si action non présente dans le controleur, alors page 404

			// Si $action existe dans $controller
			if (method_exists($controller,$action)) {
				// On enlève les indices 0 et 1 du tableau $param
				unset($param[0]);
				unset($param[1]);

				// Ont execute $action de $controller avec $param en paramètre
				call_user_func_array(array($controller,$action), $param);
			// Sinon $action non présente dans $controller
			} else {
				// Page 404
				echo 'erreur 404 (mauvaise action)';
			}
		} else {
			echo 'erreur 404 (mauvais controlleur)';
		}



	 ?>
	 <footer></footer>
	 <script
	 src="<?= WEBROOT ?>js/script.js">


	 	var url = "<?php echo $_SESSION['url']?>";
		window.onload = changeUrl(url);
	</script>
	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>
</html>
