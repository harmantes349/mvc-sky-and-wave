<?php
class CtrlUser extends Controller {
	
	 public function index() {
	 	$this->loadDao('User');
		if (isset($d['user'])) {
			$d['user'] = $this->DaoUser->read($_SESSION['id']);

			$this->set($d);
		 	$this->render('User','index');
		}
	 	else {
			
			$this->render('User','index');

		}
	 }

	 public function signIn() {
	 	$this->loadDao('User');

	 	if (!empty($this->input) && $this->DaoUser->readByEmail($this->input['email']) == null) {

	 		$salt = "2nE@5e!8";
	 		$password = sha1($this->input['password'].$salt);

	 		$user = new User($this->input['firstName'],$this->input['lastName'],$this->input['email'],$password);

	 		$newUser = $this->DaoUser->create($user);

	 		if ($newUser->getId() != "") {
	 			$d['log'] = "	<div class='alert alert-success' role='alert'>
						<h4 class='alert-heading'>Inscription réussite</h4>
					</div>";
	 		} else {
				$d['log'] = "	<div class='alert alert-danger' role='alert'>
						<h4 class='alert-heading'>Echec d'inscription dans la BDD</h4>
					</div>";
	 		}

	 		} else {
			$d['log'] = "	<div class='alert alert-warning' role='alert'>
					<h4 class='alert-heading'>Email déjà inscript ou formulaire d'inscription vide</h4>
				</div>";
	 	}
	 	$this->set($d);
	 	$this->render('User','signIn');
	 }
	 public function logIn() {
	 	$this->loadDao('User');

	 	if (!empty($this->input)) {

	 		$user = $this->DaoUser->readByEmail($this->input['email']);
				//var_dump($user) ;
	 		 	if ($user != null) {
					
	 		 		$salt = "2nE@5e!8";
	 		 		$passUser = sha1($this->input['password'].$salt);
	 		 		$passBdd = $user->getPassword();

	 		 		if ($passUser == $passBdd) {
						
	 		 			$_SESSION['id'] = $user->getId();
	 		 			$_SESSION['nom'] = $user->getLastName();
	 		 			$_SESSION['prenom'] = $user->getFirstName();
						$_SESSION['email']  = $user->getEmail();
						

	 		 			header('Location: '.WEBROOT.'User/index');
						  $this->render('User','index');
						  
						  
					  }
					  else {
						$d['log'] = " $ passUser != $ passBdd "; 
					  }
					
	 		 	} else {
					  $d['log'] = "Email incorrect";
					  
	 		 	}
	 		 } else {
				 $d['log'] = "probleme ! ";
			
			  }
			  
	 	$this->render('User','logIn');
	 }
	 public function logOut() {
		//pas propre .
	 	$_SESSION = array();
		 session_destroy();
		 header('Location: '.WEBROOT.'User/logIn');
	 	$this->render('User','logIn');
	 }
	 public function update() {}

	 public function disclamer() {
		 $this->render('User','disclamer');
	 }

	 public function admin() {
		$this->render('User','admin');
	}
}



 ?>
