<?php
require('modele/User.class.php');

class DaoUser {
	public function create($user) {
		DB::execute('INSERT INTO users (nom,prenom,email,pass) VALUES (?,?,?,?)',array($user->getLastName(),$user->getFirstName(),$user->getEmail(),$user->getPassword()));
		$user->setId(DB::lastId());
		return $user;
	}

	public function read($id) {
		$donnee = DB::selectOne('SELECT * FROM users WHERE id = ? AND archive = 0',array($id));
		if (!empty($donnee)) {
			$user = new User($donnee['prenom'],$donnee['nom'],$donnee['email'],$donnee['pass']);
			$user->setId($donnee['id']);
			return $user;
		} else {
			return null;
		}
	}

	public function readByEmail($email) {
		$donnee = DB::selectOne('SELECT * FROM users WHERE email = ? AND archive = 0',array($email));

		if (!empty($donnee)) {
			//var_dump($donnee);
			//undifine index
			$user = new User($donnee['prenom'],$donnee['nom'],$donnee['email'],$donnee['pass']);
			
			$user->setId($donnee['id']);
			return $user;
		} else {
			return null;
		}
	}
	
	public function readAll() {
		$donnees = DB::execute('SELECT * FROM users WHERE archive = 0');
		if (!empty($donnees)) {
			foreach ($donnees as $key => $donnee) {
				$tabUser[$key] = new User($donnee['prenom'],$donnee['nom'],$donnee['email'],$donnee['pass']);
				$tabUser[$key]->setId($donnee['id']);
			}
			return $tabUser;
		} else {
			return null;
		}
	}
	
	public function update($user) {
		
		DB::execute(
			'UPDATE users SET nom = ?, prenom = ?,email = ?, pass = ? WHERE id = ?',
			array($user->getLastName(),
				  $user->getFirstName(),
				  $user->getEmail(),
				  $user->getPassword(),
				  $user->getId()
			)
		);
	}
	
	public function delete($id) {
		DB::execute('UPDATE users SET archive = 1 WHERE id = ?',array($id));
	}
}

 ?>